import pandas as pd
import lib.checker_lib as cl
import importlib

importlib.reload(cl)

statement = 'test_statement.csv'
data = 'data.csv'




a, b = cl.check(data, 'citi', statement, factor=-1,
    daydelta=3, start=20200100, end=20200200)

a = pd.DataFrame(data=a)
b = pd.DataFrame(data=b)

print('\nUnmatched from personal file')
print(a)

print('\nUnmatched from bank statement')
print(b)
