

# Double entry accounting

The data files ('head.csv' and 'data.csv') and a javascript web implementation of this accounting strategy are available at 
[domestic-engineering.com](https://domestic-engineering.com/acc/web_acc/web_acc.html).

Transactions are recorded in 'data/data.csv' with a date, description, value, origination account, destination account, and a notes field.  The 'data/head.csv' file contains a list of account names, codes, and subaccounts.  In the example file included here (for a fictious, gay, Colorado, zoo keeper), that account structure described in 'head.csv' can be visualized as follows:

<p align="center">
  <img src="readme_img/readme_account_structure.svg" width="350" title="Account structure described in the head.csv example in this repo">
</p>


## Profit and loss statements

To use this library import report_lib 

run repf to produce profit and loss statements for define periods and description conditions

see 'report.py'

```
headpath = 'head.csv'
dpath = 'data.csv'

# show current account balances summary
repf('NET', False, False,
    headpath=headpath, dpath=dpath)


# show profit and loss statement for month of December 2020
repf('MMMM', True, True, month=20201200,
    headpath=headpath, dpath=dpath)


# show Citi credit card balance and previous month of transactions
# (sample transaction data ends 2020-12-31)
repf('citi', True, False, start=20201200,
    headpath=headpath, dpath=dpath)
```

To ignore transactions place an 'x' in the notes column of the transactions file.  This
is useful for balancing bank accounts.

To close an account, add 'closed' to the account name.  
    * The long form report will no longer print the account *if* the account balanced 
    is 0, the number of transactions in the report period is 0, and the show_closed bool is False.
    * The short form report will no longer print the account *if* the account balanced 
    is 0 and the show_closed bool is False.


## Graphing

see 'graph.py'

```
import matplotlib.pyplot as plt
import graph_lib as gl

gl.plot_code('NET', False, start=20200500)
plt.show()
```

## check_lib.py and checker.py

Should you fall behind on your accounting, this module can compare your bank statements to your personal accounting file.

In checker.py, we give it a bank statement, your personal accounting file, and an account code.
The bank statement needs to look like 'test_statement.csv' though it can have other columns.  The code is looking for three columns labeled 'date', 'des', and 'val'. 'test_statement.csv' is a hypothetical bank statement from January 2020 for the citi credit card in this personal accounting file.

checker.py compares all the transactions between the personal file (from January: 'start=20200100, end=20200200') to everything in the 'test_statement.csv'.  It finds that Netflix charged this card for $99.73 but the bill was recorded as $9.73. 'Drinks with coworkers' was also unmatched on both accounting lists because it was recorded as January 20th in the personal file and January 30 by the bank.  We only accept matches where the difference is less than 'daydelta' days, in this case, 3.

checker.py is not looking to match anything in the description.  The description column is only useful for the user.  checker.py is simply looking for matches between the values and date ranges of the transactions.

## Budgetting

budget_lib.py contains a couple functions for building a budget based on past history and viewing current expenses relative to a budget.


To create a budget csv based on historical data, use the following command:

```
>>> import lib.budget_lib as bl
>>> bl.export_budget('EXP_', 'current_budget.csv', year=2020, divide=12)
saved budget as current_budget.csv
>>> 
```

This line of code reads all transactions in data.csv and produces a budget ('current_budget.csv') with monthly average expenses in each category.  This CSV can be manually modified as the user prefers.  ```bl.export_budget``` can also filter with the usual 'contains' and 'does not contain' values in the description (for instance, if you wanted to ignore the purchase of a new car last year in the creation of this year's budget).

To view the current spending relative to the values in 'current_budget.csv', run the following:

```
>>> bl.repf('MMMM', month=20200500)
Balance ..........................................   -537.92               
    Expenses .........................................   2924.72           ( 84% of budget  3474.79,  550.07 remaining)
        Housing ..........................................   1301.28       (100% of budget  1302.91,    1.63 remaining)
        Healthcare .......................................    164.33       ( 56% of budget   292.30,  127.97 remaining)
        Auto/Transportation ..............................    128.16       ( 38% of budget   340.94,  212.78 remaining)
        Groceries ........................................    565.20       ( 81% of budget   701.68,  136.48 remaining)
        Misc .............................................    765.75       ( 91% of budget   836.96,   71.21 remaining)
            Misc raw .........................................    710.48   (123% of budget   576.84, -133.64 remaining)
            Food in restaurant ...............................     11.28   ( 44% of budget    25.93,   14.65 remaining)
            Dog care .........................................     43.99   ( 52% of budget    84.70,   40.71 remaining)
    Income ...........................................  -3462.64           
        City Zoo .........................................  -3462.64    
```

Note that the budget does not take time into account.  If you make a monthly budget and run ```bl.repf('MMMM', year=2020)```, the script will compare the monthly budget to the annual spending.

Budgets of sub accounts do not have to sum to the budget of the parent account.  This is purposeful as you may want a lower limit on a parent account than the sum of it's sub accounts.  This allows for flexibility over the course of the month on how to distribute the total budget while being mindful of sub categories where overspending is likely.  It also allows certain categories to remain undefined.  




