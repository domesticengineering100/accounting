import lib.report_lib as rl
import lib.budget_lib as bl
import importlib
importlib.reload(bl)
importlib.reload(rl)


 
if __name__ == '__main__':
    # repf('MMMM', long=True, filt=True, start=20200000, end=300000000, cont=[''], nc=[],
    #     headpath=headpath, dpath=dpath)

    month = 20201200
    # show current account balances summary
    rl.repf('NET', False, False)


    # show profit and loss statement for month of December 2020
    rl.repf('MMMM', True, True, month=month)

    # summary of profit and loss with comparison to numbers in 'current_budget.csv'
    bl.repf('MMMM', month=month)
    rl.saving_rate("INC", 'EXP_', month=month)

    # show Citi credit card balance and previous month of transactions
    # (sample transaction data ends 2020-12-31)
    # repf('citi', True, False, start=20201200)