import matplotlib.pyplot as plt
import lib.graph_lib as gl
import importlib
importlib.reload(gl)


# gl.plot_code('NET', True)
# gl.plot_code('NET', False, start=20230120)

fig, ax = plt.subplots(1, 1)
gl.plot_code('NET', False, start=20200000, ax=ax, trendline=45)
gl.bar_plot('gf', time_delt='month')

plt.show()