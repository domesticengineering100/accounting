import pandas as pd
from typing import List, Tuple, Optional
import lib.report_lib as rl
import importlib

importlib.reload(rl)


def tlist_to_dict(tlist: List[rl.Tran], code: str, factor: float, tfilter: rl.FilterClass) -> List[dict]:
    ll = []
    for i in tlist:
        if i.check_code(code) and tfilter.screen_tran(i) is True:
            print(type(i.dt()))
            ll.append({'date': i.dt_date(),
                    'des': i.des,
                    'val': i.inv(code)*factor})
    return ll


def find_match(line: rl.Tran, full_list: pd.DataFrame, daydelta: int) -> Tuple[Optional[int], Optional[pd.Series]]:

    for inc, i in enumerate(full_list):
        if i['val'] == line['val'] and (i['date'].date()-line['date']).days <= daydelta:
            return (inc, i)
    return (None, None)


def check(data: str, code: str, statement: str, factor: float=1, 
    daydelta: int=3, start: int=20000000, end: int=30000000) -> Tuple[List[dict], List[dict]]:
    '''

    Parameters
    ----------
    data : <str> datafile
    code : <str> account code
    statement: <str> bank statement as csv
        requires date,des,val as column names
    '''

    tlist = rl.read_trans(data, None, check=False)
    tfilter = rl.make_standard_filter(start, end)
    tlist = tlist_to_dict(tlist, code, factor, tfilter)

    st = pd.read_csv(statement, parse_dates=['date']).to_dict('records')

    unmatched_t = []

    for t in tlist:
        inc, i = find_match(t, st, daydelta)
        if i is None:
            unmatched_t.append(t)
        else:
            st.remove(i)
    # return unmatched transactions in personal file, and unmatched transactions on statement
    return unmatched_t, st