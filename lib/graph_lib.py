import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import pandas as pd
from typing import List, Literal
import lib.report_lib as rl


from datetime import datetime as dt
from datetime import timedelta


def allchild(acc: dict, code: str) -> List[str]:
    ''' find all children of account code
    
    Parameters
    ----------
    acc         : <list of Account>
    code        : <str>
   
    Returns
    -------
    <list of str> 
        account codes
    '''

    kids = []
    kids.extend(acc[code].children)
    for c in acc[code].children:
        kids.extend(allchild(acc, c))
    return kids


def plotter(code: str, tlist: List[rl.Tran], acc: dict, filt: bool=True,
    tfilter: rl.FilterClass=rl.FilterClass(), ax=None, trendline: int=0,
    **kwarggs) -> None:

    def sign(t, codes):
        if t.to in codes and t.fr in codes:
            return 0
        elif t.to not in codes and t.fr not in codes:
            return 0
        elif t.to in codes:
            return t.val
        elif t.fr in codes:
            return -t.val

    if ax is None:
        fig, ax = plt.subplots(1, 1)

    kids = allchild(acc, code)
    kids.append(code)

    tfilt_list = [t for t in tlist if tfilter.screen_tran(t) is True]
    tfilt_list.sort(key=lambda x: x.date)

    dates = np.array([i.dt() for i in tfilt_list])
    delts = np.array([sign(i, kids) for i in tfilt_list])
    vals = np.cumsum(delts)
    xx = np.concatenate(([dates[0]-timedelta(days=1)],dates))
    y = np.concatenate(([0], vals))

    
    if not filt:
        y += rl.acc_total(acc, code, True) - y[-1]
    
    ax.step(xx, y, where='post', label=acc[code].name, **kwarggs)

    if trendline > 0:
        df = pd.DataFrame(data={'dates': xx, 'values': y})
        df.index = df['dates']
        df = df['values'].rolling(pd.Timedelta(trendline, "d")).median()
        ax.plot(df.index, df, '--', **kwarggs)
    ax.legend(loc='upper left')

    formatter = ticker.StrMethodFormatter('${x:,.2f}')
    ax.yaxis.set_major_formatter(formatter)
    ax.set_ylabel('Account total')
    ax.set_xlabel('Date')

    return (xx, y)


def plot_code(code: str, filt: bool=True, start: int=0, 
    end: int=300000000, cont: List[str]=[''], nc: List[str]=[],
    headpath: str=rl.headpath, dpath: str=rl.dpath, ax=None, trendline: int=0,
    **kwarggs) -> None:
    ''' Run graph function
    
    Parameters
    ----------
    code        : <str>
    filt        : <bool>
        filter totals (T/F)
    start       : <int>
        start date as int 2020-12-08 as 20201208
    end         : <int>
        end date as int 2020-12-08 as 20201208
    cont        : <list of str>
        t.des must contain any of the strings in cont
    nc          : <list of str>
        t.des must not contain any strings in nc
    headpath    : <str>
        path to header file
    dpath       : <str>
        path to transaction file
    ax          : <pyplot.axes> or None
    
    Returns
    -------
    None
    '''
    
    tfilter = rl.make_standard_filter(start, end, cont, nc)
    tlist, acc, _ = rl.reader(tfilter, headpath, dpath)
    tlist.sort(key =lambda x: x.date)
    output = plotter(code, tlist, acc, filt=filt, tfilter=tfilter, ax=ax, trendline=trendline, **kwarggs)
    return output


def dateinc_month(date: dt) -> dt:
    ''' takes dt.datetime and returns dt.datetime + 1 month
    
    Parameters
    ----------
    date        : <dt.datetime>

    Returns
    -------
    <dt.datetime> incremented by one month

    '''
    if date.month != 12:
        return dt(date.year, date.month+1, 1)
    else:
        return dt(date.year+1, 1, 1)


def arange_date(dates: List[dt], time_delt: Literal['month', 'year']='month') -> np.ndarray:
    ''' from list of dates, creates an evenly spaced array by month
    containing the min and max of date

    Parameters
    ----------
    dates       : <list of dt.datetimes>
    time_delt   : <str>
        'month' or 'year'

    Returns
    -------
    <list of dt.datetimes>
    '''

    if time_delt == 'month':
        nd = [dt(year=np.min(dates).year, month=np.min(dates).month, day=1)]
        while np.max(nd) < np.max(dates):
            nd.append(dateinc_month(nd[-1]))
    if time_delt == 'year':
        nd = [dt(year=np.min(dates).year, month=1, day=1)]
        while np.max(nd) < np.max(dates):
            nd.append(dt(nd[-1].year+1, 1, 1))
    return np.array(nd)


def bar_plot(code: str, time_delt: Literal['month', 'year']='month', start: int=0, 
    end: int=300000000, cont: List[str]=[''], nc: List[str]=[],
    headpath: str=rl.headpath, dpath: str=rl.dpath, ax=None) -> None:
    ''' bar plot graph utility 
    
    Parameters
    ----------
    code        : <str>
    time_delt   : <str>
        'month' or 'year'
    start       : <int>
        start date as int 2020-12-08 as 20201208
    end         : <int>
        end date as int 2020-12-08 as 20201208
    cont        : <list of str>
        t.des must contain any of the strings in cont
    nc          : <list of str>
        t.des must not contain any strings in nc
    headpath    : <str>
        path to header file
    dpath       : <str>
        path to transaction file
    ax          : <pyplot.axes> or None
    
    Returns
    -------
    None
    '''

    if ax is None:
        fig, ax = plt.subplots(1, 1)

    tfilter = rl.make_standard_filter(start, end, cont, nc)
    tlist, acc, _ = rl.reader(tfilter, headpath, dpath)


    kids = allchild(acc, code)
    kids.append(code)

    fval = {'start': start, 'end': end, 'cont': cont, 'nc': nc}
    tfilt_list = [t for t in tlist if tfilter.screen_tran(t)]

    dates = [t.dt() for t in tfilt_list]
    xx = arange_date(dates, time_delt)
    y = np.zeros(len(xx))
    for t in tfilt_list:
        if t.to in kids:
            i = np.argmax(xx > t.dt())
            if i > 0:
                y[i-1] += t.val
        if t.fr in kids:
            i = np.argmax(xx > t.dt())
            if i > 0:
                y[i-1] += -t.val

    lw = 10 if time_delt.lower() == 'month' else 100
    ax.bar(xx, y, width=lw, label=acc[code].name)
    ax.legend(loc='upper left')
    formatter = ticker.FormatStrFormatter('$%1.2f')
    ax.yaxis.set_major_formatter(formatter)
    ax.set_ylabel('Account total')
    ax.set_xlabel('Date')



def saving_rate(inc_code, exp_code, tlist, acc, filt: bool, tfilter, 
    time_delta: Literal['month', 'year']='year', ax=None, **kwarggs):
    def sign(t, codes):
        if t.to in codes and t.fr in codes:
            return 0
        elif t.to not in codes and t.fr not in codes:
            return 0
        elif t.to in codes:
            return t.val
        elif t.fr in codes:
            return -t.val

    def total_by_period(tlist, time_delta):
        start_date = tlist[0].dt()
        end_date = tlist[-1].dt()
        ts = []
        if time_delta == 'year':
            inc = np.arange(start_date.year, end_date.year+1)
            for i in inc:
                ts.append([j for j in tlist if j.dt().year==i])
            x = [dt(i, 1, 1) for i in inc]
        elif time_delta == 'month':
            x = pd.date_range(dt(start_date.year, start_date.month, 1), end_date, freq='MS')
            for i in x:
                ts.append([j for j in tlist if j.dt().year == i.year and j.dt().month == i.month])
        return x, ts

    inc_kids = allchild(acc, inc_code)
    inc_kids.append(inc_code)
    exp_kids = allchild(acc, exp_code)
    exp_kids.append(exp_code)

    if filt is True:
        tfilt_list = [t for t in tlist if tfilter.screen_tran(t)]

    
    x, grouped = total_by_period(tfilt_list, time_delta)
    yexp = np.array([np.sum([sign(i, exp_kids) for i in per]) for per in grouped])
    yinc = np.array([np.sum([sign(i, inc_kids) for i in per]) for per in grouped])
    rate = 1 - np.abs(yexp / yinc)

    if ax is None:
        fig, ax = plt.subplots(1, 1)
    ax.plot(x, rate*100, 'o')
    ax.plot(x, np.zeros(len(x)), color='xkcd:grey', alpha=0.2)
    ax.set_ylabel('Savings rate (%)')
    ax.set_xlabel('Date')
