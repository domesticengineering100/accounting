import lib.report_lib as rl
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from dataclasses import dataclass
from typing import List, Tuple

''' WIP - report functions to track oil changes and gas milesage based on description fields for 
auto transactions
'''


@dataclass
class OilChange(rl.Tran):
    car_code: str
    vendor: str
    nextchange: int

@dataclass
class GasTank(rl.Tran):
    odometer: int
    tripmeter: float
    gallons: float


def car_stats(car_code: str, tlist: List[rl.Tran]) -> Tuple[List[OilChange], List[GasTank]]:
    ''' for a given car code, parses tlist into a list of OilChange and GasTank objects.
    '''

    def makeOilChange(t: rl.Tran):
        b = [i.strip() for i in t.des.split('-')]
        nc = int(b[3].replace('next change', '').strip())
        return OilChange(t.date, t.des, t.val, t.fr, t.to, t.notes, t.bare, b[0], b[2],  nc)
    
    def makeGasTank(t: rl.Tran):
        b = [i.strip() for i in t.des.split('-')]
        
        try: 
            odo = int(b[5])
        except ValueError:
            odo = np.nan

        try:
            tank = float(b[4])
        except ValueError:
            tank = np.nan

        try: 
            gal = float(b[3])
        except ValueError:
            gal = np.nan
        return GasTank(t.date, t.des, t.val,t.fr, t.to, t.notes, t.bare, odo, tank, gal)

    oil_changes = [makeOilChange(t) for t in tlist if car_code in t.des and 'Oil' in t.des and 'next change' in t.des]
    oil_changes.sort(key=lambda x: x.date)
    gas = [makeGasTank(t) for t in tlist if car_code in t.des and 'Gas' in t.des]
    gas.sort(key=lambda x: x.date)
    return (oil_changes, gas)


def next_oil_change(oil_changes: List[OilChange], gas: List[GasTank]) -> float:
    ''' given a list of oil changes and list of gas tanks, this function returns 
        the number of miles until the next oil change '''
    odos = np.array([i.odometer for i in gas])
    odos = odos[odos == odos]
    return oil_changes[-1].nextchange - odos[-1]


def mileage_tracker(gas: List[GasTank]) -> pd.DataFrame:
    ''' create dataframe for plotting gas mileage '''

    df = pd.DataFrame(data=[{'date': i.dt(),
                             'gallons': i.gallons,
                             'tripmeter': i.tripmeter,
                             'odometer': i.odometer,
                             'cost': i.val} for i in gas])
    df['odo_trip'] = df['odometer'] - df['odometer'].shift(1)
    df['mileage'] = df['tripmeter'] / df['gallons']
    df['gal_cost'] = df['cost'] / df['gallons']
    df['data_valid'] = (df['tripmeter'] - df['odo_trip']).abs() < 5
    return df 


def plot_car(df: pd.DataFrame) -> Figure:
    fig, axe = plt.subplots(3, 1, sharex=True)
    df = df.loc[df['data_valid']]

    ax = axe[2]
    ax.plot(df['date'], df['gallons'], 'o')
    
    ax.set_ylabel('Gas volume (gallons)')

    ax.set_ylim(bottom=0)
    ax2 = ax.twinx()
    ax2.plot(df['date'].iloc[0], [-1000], 'o', label='Gas volume')
    ax2.plot(df['date'], df['tripmeter'], 'o', color='xkcd:red', label='Trip meter')
    ax2.set_ylabel('Distance per tank (miles)')
    ax2.set_ylim(bottom=0)
    ax2.legend(loc='lower left')

    ax = axe[1]
    ax.plot(df['date'], df['gal_cost'], 'o')
    ax.set_ylabel('Gas cost ($/gal)')
    ax.set_ylim(bottom=0)

    ax = axe[0]
    ax.plot(df['date'], df['mileage'], 'o')
    ax.set_ylabel('Gas mileage (mi/gal)')

    return fig
