from datetime import datetime, date
import pandas as pd
from dataclasses import dataclass, field
import shutil
from pathlib import Path
from typing import List, Optional, Tuple, Callable, Union

str_or_ls = Union[str, List[str]]
delim = ','
headpath = 'data/head.csv'
dpath = 'data/data.csv'

@dataclass
class Tran:
    date: int
    des: str
    val: float
    fr: str 
    to: str
    notes: str
    bare: str

    def date_str(self) -> str:
        s = str(self.date)
        return s[:4] + '-' + s[4:6] + '-' + s[6:8]

    def dt(self) -> datetime:
        s = str(self.date)
        return datetime(int(s[:4]), int(s[4:6]), int(s[6:8]))

    def dt_date(self) -> date:
        return self.dt().date()
    
    def check_code(self, code: str) -> bool:
        return (code == self.fr) or (code == self.to)

    def inv(self, code: str) -> float:
        if code == self.fr:
            return -self.val
        if code == self.to:
            return self.val
        else:
            return 0

    def hash(self) -> str:
        return str(self.date)+self.des+str(int(self.val))+ self.to+self.fr

@dataclass
class FilterClass:
    '''  All filter type classes must inheirit FilterClass to prove that the 
    the class has a screen_tran function which takes a single transaction
    as an argument.  In the future, we may define arbitrary class to work as filters
    but for now, the only one in this file of practical use is StandardFilter.
    '''
    pass
    def screen_tran(self, transaction: Tran) -> bool:
        return True

@dataclass
class StandardFilter(FilterClass):
    start: int
    end: int
    cont: List[str]
    nc: List[str]

    def screen_tran(self, transaction: Tran) -> bool:
        if any([i in transaction.des for i in self.cont]) and \
            all([i not in transaction.des for i in self.nc]) and \
            transaction.date >= self.start and transaction.date < self.end:
            return True
        else:
            return False
        
@dataclass
class Account:
    name: str
    children: List[str]
    code: str
    raw_value: float = 0
    filter_value: float = 0
    tlist: List = field(default_factory=lambda: [])

    def add_trans(self, t: Tran, tfilter: FilterClass) -> None:
        ''' function to screen and incorporate transactions into tlist and account value'''
        if tfilter.screen_tran(t) is True:
            self.filter_value += t.inv(self.code)
            self.tlist.append(t)
        self.raw_value += t.inv(self.code)

    def value(self, raw: bool=True) -> float:
        if raw:
            return self.raw_value
        else:
            return self.filter_value


def make_standard_filter(start: int=0, end: int=300000000,
    cont: str_or_ls=[''], nc: str_or_ls=[],
    month: Optional[int]=None, year: Optional[int]=None) -> StandardFilter:
    if type(cont) is str:
        cont = [cont]
    if type(nc) is str:
        nc = [nc]
    if month is not None:
        start = month
        end = month + 100
    if year is not None:
        start = year * 10000
        end = year * 10000 + 10000
    return StandardFilter(start, end, cont, nc)


def make_Tran(ll: str, sep: str=delim) -> Tran:
    line = [i.strip() for i in ll.split(sep)]
    date = line[0].replace('-', '')
    if len(date) != 8:
        raise ValueError('Date has incorrect format "%s"' % ll)
    des = line[1]
    val = float(line[2])
    if len(line) == 6:
        return Tran(int(date), des, val, line[3], line[4], line[5], ll)
    else:
        return Tran(int(date), des, val, line[3], line[4], '', ll)


def check_codes(accounts, i: Tran) -> None:
    ''' checks that all codes in a transaction line appear in the header file
    
    Parameters
    ----------
    accounts    : <list of Account objects>
    i           : < Tran object>

    Returns
    -------
    None    
    '''

    codes = set([j for j in accounts])
    if i.to not in codes or i.fr not in codes:
        print('invalid account code')
        print(i.bare)
    if i.to == i.fr:
        print('matching account to and from')
        print(i.bare)

 
def read_trans(path: str, accounts: dict, check=True) -> List[Tran]:
    ''' reads the transactions file, checks that codes are in header file
    
    Parameters
    ----------
    path        : <str> path to csv
    accounts    : <list of Account objects>

    Returns
    -------
    <list of Tran objects>
    '''

    lil = []
    with open(path) as f:
        for l in f:
            try:
                t = make_Tran(l)
                if t.notes.lower().strip() != 'x':
                    if check is True:
                        check_codes(accounts, t)
                    lil.append(t)
            except ValueError:
                pass
    return lil


def read_head(path: str) -> dict:
    ''' reads the header file
    
    Parameters
    ----------
    path        : <str> path to csv

    Returns
    -------
    <list of Account objects>
    '''

    lil = {}
    with open(path) as f:
        for line in f:
            l = [i.strip() for i in line.split(delim)]
            if line.strip() != '' and len(l) == 3:
                kids = l[2].split(';')
                if '' in kids:
                    kids.remove('')
                kids = kids if len(kids) > 0 else []
                a = Account(l[1].replace('_',' ').strip(),
                            kids,
                            l[0])
                lil[a.code] = a
    return lil


def reader(tfilter: FilterClass, headpath: str=headpath,
           dpath: str=dpath, read_head: Callable=read_head, 
           read_trans: Callable=read_trans) -> Tuple[List[Tran], dict, int]:
    ''' read the header file and the transactions file,
    builds accounts with filtering information
    
    Parameters
    ----------
    start       : <int>
        start date as int 2020-12-08 as 20201208
    end         : <int>
        end date as int 2020-12-08 as 20201208
    cont        : <list of str>
        t.des must contain any of the strings in cont
    nc          : <list of str>
        t.des must not contain any strings in nc
    headpath    : <str>
        path to header file
    dpath       : <str>
        path to transaction file
    
    Returns
    -------
    <list of Tran>, <list of Accounts>
    '''

    acc = read_head(headpath)
    tlist = read_trans(dpath, acc)

    acc = build_accounts(acc, tlist, tfilter)
    filt_count = len([t for t in tlist if tfilter.screen_tran(t)])

    return tlist, acc, filt_count


def build_accounts(acc: dict, tlist: List[Tran], tfilter: FilterClass) -> dict:
    ''' add transactions to account objects
    
    Parameters
    ----------
    acc         : <list of Account objects>
    tlist       : <list of Tran objects>
    fval        : <dict>
        {'cont': <list>, 'nc': <list>, 'start': <int>, 'end': <int>}
    
    Returns
    -------
    <list of Account objects>
    '''

    for t in tlist:
        acc[t.to].add_trans(t, tfilter)
        acc[t.fr].add_trans(t, tfilter)
    return acc


def acc_total(acc: dict, code: str, raw: bool) -> float:
    ''' get total account value from sub accounts
    
    Parameters
    ----------
    acc         : <list of Account objects>
    code        : <str>
    raw         : <bool>
    
    Returns
    -------
    <float>
    '''

    v = acc[code].value(raw)
    for i in acc[code].children:
        v += acc_total(acc, i, raw)
    return v


def line_count(acc: dict, code: str) -> Tuple[int, int]:
    ''' get line count from sub accounts
    
    Parameters
    ----------
    acc         : <list of Account objects>
    code        : <str>
    
    Returns
    -------
    <int>, <int>
    (# of lines in self and all children,
        # of lines in self)
    '''
    v = len(acc[code].tlist)
    for i in acc[code].children:
        v += line_count(acc, i)[0]
    return (v, len(acc[code].tlist))


def linecount_string(fullc: int, subc: int, children: list, xls: bool) -> str:
    ''' decide how to display line count

    Parameters
    ----------
    fullc       : <int>
        # of lines in self and all children
    subc        : <int>
        # of lines in self
    children    : <list of str>
        list of all child accounts
    xls         : <bool>
        print mode in csv or human readable

    Returns
    -------
    <str>
    '''

    sep = ' - ' if xls else ', '
    if fullc < 4:
        return ''
    elif len(children) == 0:
        return ' - %.0f' % fullc
    else:
        return ' - %.0f' % fullc + sep + '%.0f' % subc


def check_show_closed(show: bool, fullc: int, val: float, name: str) -> bool:
    if show is True or abs(val) > 0.00001 or fullc > 0 or 'closed' not in name:
        return True
    else:
        return False


def repfunc(code: str, acc: dict, raw: bool, show0s: bool, 
    show_closed: bool, xls: bool, show_origin: bool, ind: int=0) -> None:
    ''' Run long form report function
    
    Parameters
    ----------
    code        : <str>
    acc         : <list of Account objects>
    raw         : <bool>
    show0s      : <bool>
    toggle whether to print accounts with 0 balance and 0 transactions
    xls         : <bool>
        toggle print mode from csv to human parsable
    ind         : <int>

    Returns
    -------
    None
    '''

    fullc, subc = line_count(acc, code)
    val = acc_total(acc, code, raw)
    cnt = linecount_string(fullc, subc, acc[code].children, xls)
    if fullc != 0 or abs(val) > 0.00001 or show0s or ind == 0:
        if check_show_closed(show_closed, fullc, val, acc[code].name) is True:
            if xls:
                print((','*ind) + acc[code].name + ',' + cnt + ',' + '%10.2f' % val)
                for t in sorted(acc[code].tlist, key=lambda x: x.date):
                    if show_origin is True:
                        suffix_str = t.to if t.to != code else t.fr
                        suffix_str = ',' + suffix_str
                    else:
                        suffix_str = ''
                    print((','*ind) + t.date_str() + ',' + t.des + ',%10.2f' % t.inv(code)+suffix_str)
                    print((','*(ind)) + ',' + 'Total ,'+ '%12.2f' % val)
                for c in acc[code].children:
                    repfunc(c, acc, raw, show0s, show_closed, xls, ind=ind+1)
            else:
                
                print((' '*ind) + acc[code].name + cnt)
                for t in sorted(acc[code].tlist, key=lambda x: x.date):
                    if show_origin is True:
                        suffix_str = t.to if t.to != code else t.fr
                        suffix_str = suffix_str.rjust(10, ' ')
                    else:
                        suffix_str = ''
                    print((' '*ind) + t.date_str() + ' ' + t.des.ljust(80, '.') + '%10.2f' % t.inv(code) + suffix_str)
                if subc > 1 and fullc != subc:
                    print((' '*(ind+10)) + ' ' + 'Subtotal '.ljust(80, '.') + '%12.2f' % acc[code].value(raw))
                for c in acc[code].children:
                    repfunc(c, acc, raw, show0s, show_closed, xls, show_origin, ind=ind+4)
                print((' '*(ind+10)) + ' ' + 'Total '.ljust(80, '.') + '%12.2f' % val)


def srepfunc(code: str, acc: dict, raw: bool, show0s: bool, 
    show_closed: bool, xls: bool, ind: int=0) -> None:
    ''' Run short form report function
    
    Parameters
    ----------
    code        : <str>
    acc         : <list of Account objects>
    raw         : <bool>
    show0s      : <bool>
        toggle whether to print accounts with 0 balance and 0 transactions
    xls         : <bool>
        toggle print mode from csv to human parsable
    ind         : <int>

    Returns
    -------
    None
    '''

    fullc, _ = line_count(acc, code)
    val = acc_total(acc, code, raw)
    if fullc != 0 or val > 0.00001 or show0s:
        if show_closed is True or abs(val) > 0.00001 or 'closed' not in acc[code].name:
            if xls:
                print((','*ind) + acc[code].name + ',' + '%10.2f' % val)
            else:
                print((' '*ind) + (acc[code].name + ' ').ljust(50, '.') + f'{val:,.2f}'.rjust(16, ' '))
            for c in acc[code].children:
               srepfunc(c, acc, raw, show0s, show_closed, xls, ind=ind+(1 if xls else 4))


def repf(code: str, long: bool, filt: bool=True, start: int=0, end: int=300000000,
    cont: str_or_ls=[''], nc: str_or_ls=[],
    headpath: str=headpath, dpath: str=dpath, month: Optional[int]=None, 
    year: Optional[int]=None, show0s: bool=False,
    xls: bool=False, show_closed: bool=False, tfilter: Optional[FilterClass]=None, 
    show_origin: bool=False) -> None:
    ''' Run report function to print profit and loss statements on periods of time,
    balance credit cards and bank accounts, etc
    
    Parameters
    ----------
    code        : <str>
    long        : <bool>
        print long form (T/F)
    filt        : <bool>
        filter totals (T/F)
    start       : <int>
        start date as int 2020-12-08 as 20201208
    end         : <int>
        end date as int 2020-12-08 as 20201208
    cont        : <list of str>
        t.des must contain any of the strings in cont
    nc          : <list of str>
        t.des must not contain any strings in nc
    headpath    : <str>
        path to header file
    dpath       : <str>
        path to transaction file
    month       : <int>
        auto set start and end to one month
        20201100 would show November 2020
    year        : <int>
        auto set start and end to one year
        20200000 would show year 2020
    show0s      : <bool>
        toggle whether to print accounts with 0 balance and 0 transactions
        default False
    xls         : <bool>
        toggle print mode from csv to human parsable
        default False
    
    
    Returns
    -------
    None
    '''

    if tfilter is None:
        tfilter = make_standard_filter(start, end, cont, nc, month, year)

    tlist, acc, filt_count = reader(tfilter, headpath, dpath)
    if long:
        repfunc(code, acc, not filt, show0s, show_closed, xls, show_origin, ind=0)
    else:
        srepfunc(code, acc, not filt, show0s, show_closed, xls, ind=0)
    
    print('\n%.0f total transactions, %.0f screened transactions\n\n' % (len(tlist), filt_count))


def findX(dpath: str=dpath) -> None:
    ''' print transactions which have x in the notes

    Parameters
    ----------
    dpath       : <str>
        path to transaction file

    Returns
    -------
    None
    '''

    df = pd.read_csv(dpath)
    df = df.astype({'notes': str})
    print(df.loc[df['notes'].str.lower().str.strip() == 'x'])


def saving_rate(inc_code: str, exp_code: str, start: int=0, 
    end: int=300000000, cont: str_or_ls=[''], nc: str_or_ls=[],
    month: Optional[int]=None, year: Optional[int]=None, 
    tfilter: Optional[FilterClass]=None) -> None:
    
    if tfilter is None:
        tfilter = make_standard_filter(start, end, cont, nc, month, year)
    _, acc, _ = reader(tfilter, headpath, dpath)
    _save_rate(inc_code, exp_code, acc)


def _save_rate(inc_code: str, exp_code: str, acc: List[Account]):
    income = acc_total(acc, inc_code, False)
    expenses = acc_total(acc, exp_code, False)
    print(f'Income ${income:.2f}, expenses ${expenses:.2f}')
    print(f'Savings rate = {(1-abs(expenses/income))*100:.2f}%')


def backup(dpath: str, headpath: str, savepath: str):
    ''' creates an automatic backup of the data and header files
    
    Parameters
    ----------
    dpath    : <str>
    headpath : <str>
    savepath : <str> 
        '/media/mike/Samsung_T3/Accounting/backups' for example

    Notes
    -----
    While this could be accomplished many ways, it is implemented here 
    as a direct copy to another directory with today's date in the file name
    The backup directory will accumulate files that represent a snapshot 
    of the last time this function was called each day.  While this is not 
    a terribly efficient way to do backups, these files are small (less than 3 MB
    for 12 years of my personal financial data).
    '''

    datapath = date.today().strftime('%Y-%m-%d_') + Path(dpath).stem + '.csv'
    datapath = Path(savepath).joinpath(datapath)
    head = date.today().strftime('%Y-%m-%d_') + Path(headpath).stem + '.csv'
    head = Path(savepath).joinpath(head)

    shutil.copyfile(dpath, datapath)
    shutil.copyfile(headpath, head)
    print('Backup successful to ', savepath)
