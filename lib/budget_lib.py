import lib.report_lib as rl
import pandas as pd
import numpy as np
from typing import List, Optional
import importlib
importlib.reload(rl)

budget_path = 'data/current_budget.csv'
'''  functions to build 'current_budget.csv'   
'''

def build_budget(code: str, headpath: str=rl.headpath, dpath: str=rl.dpath, divide: float=12,
    start: int=20200000, end: int=20210000, cont: List[str]=[''], nc: List[str]=[]) -> None:
    ''' makes a dataframe of budget values and account codes using account
    values from 'start' to 'end' divided by 'divide'

    normally would set start and end to a 12 month period and divide is 12

    Parameters
    ----------
    code        : <str> parent account for expenses to budget
    '''

    tlist, acc, filt_count = rl.reader(start=start, end=end, cont=cont, nc=nc, headpath=headpath, dpath=dpath)
    codelist = []
    def addcode(acc, code):
        if len(acc[code].children) == 0:
            codelist.append({'code': code,
                             'budget': acc[code].filter_value/divide})
        else:
            codelist.append({'code': code,
                             'budget': rl.acc_total(acc, code, False)/divide})
            codelist.append({'code':code+'_raw',
                             'budget': acc[code].filter_value/divide})
        for i in acc[code].children:
            addcode(acc, i)
    addcode(acc, code)
    return pd.DataFrame(data=codelist)


def export_budget(code: str, path: str, headpath: str=rl.headpath, dpath: str=rl.dpath, divide: float=12,
    start: int=20200000, end: int=20210000, cont: List[str]=[''], 
    nc: List[str]=[], month: Optional[int]=None, year: Optional[int]=None) -> None:
    '''  saves budget using account values from 'start' to 'end' divided by 'divide'
    normally would set start and end to a 12 month period and divide is 12

    Parameters
    ----------
    code        : <str> parent account for expenses to budget
    path        : <str> path to save budget dataframe

    Returns
    -------
    None
    '''
    if type(cont) is str:
        cont = [cont]
    if type(nc) is str:
        nc = [nc]
    if month is not None:
        start = month
        end = month + 100
    if year is not None:
        start = year * 10000
        end = (year + 1) * 10000
    
    budget_df = build_budget(code=code, headpath=headpath, dpath=dpath,
                             divide=divide, start=start, end=end, cont=cont, nc=nc)
    budget_df.to_csv(path, sep=',', index=False)
    print('saved budget as ', path)


''' modification of repf to show expenses against 'current_budget.csv' numbers 
'''


def read_budget_to_acc(budget_path: str, acc: dict) -> dict:
    '''  adds 'total_budget' and 'raw_budget' to rl.Account objects

    reads budget from 'budget_path' (expecting a csv)

    '''
    def tofloat(x):
        try:
            return float(x)
        except:
            return np.nan
    
    df = pd.read_csv(budget_path)
    df['budget'] = df['budget'].map(tofloat)
    bud = dict(zip(df['code'], df['budget']))
    for a in acc.values():
        if a.code in bud.keys() and bud[a.code] == bud[a.code]:
            a.total_budget = bud[a.code]
        else:
            a.total_budget = None
        if a.code+'_raw' in bud.keys() and bud[a.code+'_raw'] == bud[a.code+'_raw']:
            a.raw_budget = bud[a.code+'_raw']
        else:
            a.raw_budget = None
    return acc


def srepfunc(code: str, acc: dict, raw: bool, show0s: bool, show_closed: bool, ind: int=0):
    ''' Run short form report function
    
    Parameters
    ----------
    code        : <str>
    acc         : <list of Account objects>
    raw         : <bool>
    show0s      : <bool>
        toggle whether to print accounts with 0 balance and 0 transactions
    ind         : <int>

    Returns
    -------
    None
    '''

    total_length = 75

    fullc, _ = rl.line_count(acc, code)
    val = rl.acc_total(acc, code, raw)
    rawval = acc[code].raw_value if raw else acc[code].filter_value

    if acc[code].total_budget is not None:
        val_suffix = f'({val/acc[code].total_budget*100:3.0f}% of budget {acc[code].total_budget:8.2f}, {acc[code].total_budget-val:7.2f} remaining)'
    else:
        val_suffix = ''
    if acc[code].raw_budget is not None and acc[code].raw_budget != 0:
        rawval_suffix = f'({rawval/acc[code].raw_budget*100:3.0f}% of budget {acc[code].raw_budget:8.2f}, {acc[code].raw_budget-rawval:7.2f} remaining)'
    else:
        rawval_suffix = ''
    
    if fullc != 0 or val > 0.00001 or show0s:
        if show_closed is True or abs(val) > 0.00001 or 'closed' not in acc[code].name:
            print(((' '*ind) + (acc[code].name + ' ').ljust(50, '.') + '%10.2f ' % val).ljust(total_length, ' ') + val_suffix)
            if len(acc[code].children) > 0 and abs(rawval) > 0.0001:
                print(((' '*(ind+4)) + (acc[code].name + ' raw ').ljust(50, '.') + '%10.2f ' % rawval).ljust(total_length, ' ') + rawval_suffix)
            for c in acc[code].children:
               srepfunc(c, acc, raw, show0s, show_closed, ind=ind+4)


def repf(code: str, filt: bool=True, start: int=0, end: int=300000000, 
    cont: List[str]=[''], nc: List[str]=[],
    headpath: str=rl.headpath, dpath: str=rl.dpath, budget_path: str=budget_path, 
    month: Optional[int]=None, year: Optional[int]=None, show0s: bool=False, 
    show_closed: bool=False, tfilter: Optional[rl.FilterClass]=None) -> dict:
    ''' Run report function to print profit and loss statements on periods of time,
    balance credit cards and bank accounts, etc
    
    Parameters
    ----------
    code        : <str>
    filt        : <bool>
        filter totals (T/F)
    start       : <int>
        start date as int 2020-12-08 as 20201208
    end         : <int>
        end date as int 2020-12-08 as 20201208
    cont        : <list of str>
        t.des must contain any of the strings in cont
    nc          : <list of str>
        t.des must not contain any strings in nc
    headpath    : <str>
        path to header file
    dpath       : <str>
        path to transaction file
    month       : <int>
        auto set start and end to one month
        20201100 would show November 2020
    year        : <int>
        auto set start and end to one year
        20200000 would show year 2020
    show0s      : <bool>
        toggle whether to print accounts with 0 balance and 0 transactions
        default False
    
    Returns
    -------
    None
    '''

    if tfilter is None:
        tfilter = rl.make_standard_filter(start, end, cont, nc, month, year)
    tlist, acc, filt_count = rl.reader(tfilter, headpath, dpath)
    acc = read_budget_to_acc(budget_path, acc)
    srepfunc(code, acc, not filt, show0s, show_closed, ind=0)
    
    print('\n%.0f total transactions, %.0f screened transactions\n\n' % (len(tlist), filt_count))
    return acc

